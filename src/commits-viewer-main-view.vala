namespace CommitsViewer
{
	[GtkTemplate (ui = "/org/gnome/commits-viewer/ui/commits-viewer-main-view.ui")]
	public class MainView : Gtk.Box
	{
		[GtkChild]
		private Gtk.Entry d_add_project_entry;
		
		public Gtk.Entry add_project_entry
		{
			get { return d_add_project_entry; }
		}
		
		[GtkChild]
		private Gtk.Button d_add_project_button;
		
		public Gtk.Button add_project_button
		{
			get { return d_add_project_button; }
		}
	}
}
