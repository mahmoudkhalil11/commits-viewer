namespace CommitsViewer
{
	public enum ModelItems
	{
		COLUMN_NAME,
		COLUMN_NUMBER,
		N_COLUMNS,
	}
	
	[GtkTemplate (ui = "/org/gnome/commits-viewer/ui/commits-viewer-commits-list-view.ui")]
	public class CommitsListView : Gtk.Box
	{
		[GtkChild]
		private Gtk.TreeView d_commits_tree_view;
		
		public Gtk.TreeView commits_tree_view
		{
			get { return d_commits_tree_view; }
		}
		
		private Gtk.ListStore d_list_store;
		
		construct
		{	
			init();
		}
		
		private void init()
		{
			d_list_store = new Gtk.ListStore(ModelItems.N_COLUMNS, GLib.Type.STRING, GLib.Type.INT);
			
			d_commits_tree_view.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE);
			d_commits_tree_view.get_selection().changed.connect((sel) => {
			    Gtk.TreePath path;
			    d_commits_tree_view.get_cursor(out path, null);
			    print("sel count: %d\n", sel.count_selected_rows());

			    if(sel.count_selected_rows() > 2)
			    {
			        sel.unselect_all();
			        sel.select_path(path);
			    }
			});
		}
		
		public void load_model(Ggit.Repository repo)
		{
			Gtk.TreeIter iter;
			Ggit.RevisionWalker walker = null;
			try
			{
				walker = new Ggit.RevisionWalker(repo);
			
				
			
				if(walker == null)
					return;
					
				walker.reset();
				walker.set_sort_mode(Ggit.SortMode.REVERSE);
				walker.push_head();

				/*for(int i = 0; i < 10; i++)
				{
					print("here\n");
					var oid = walker.next();
					print(oid.to_string() + "\n");
					var commit = repo.lookup_commit(oid);
					d_list_store.append(out iter);
					d_list_store.set(iter, ModelItems.COLUMN_NAME, commit.get_subject, ModelItems.COLUMN_NUMBER, oid);
				}*/
				
				var counter = 0;
				do
				{
					var oid = walker.next();
					if(oid == null)
						break;
					var commit = repo.lookup_commit(oid);
					d_list_store.append(out iter);
					d_list_store.set(iter, ModelItems.COLUMN_NAME, commit.get_subject(), ModelItems.COLUMN_NUMBER, oid);
					counter++;
				} while(counter < 10);
				
				d_commits_tree_view.set_model(d_list_store);
				
				Gtk.CellRenderer renderer;
				
				renderer = new Gtk.CellRendererText();
				d_commits_tree_view.insert_column_with_attributes(-1, "Commit", renderer, "text", ModelItems.COLUMN_NAME);
			}
			catch(Error err)
			{
				print(err.message + "\n");
			}
		}
	}
}
